# balena-pihole

This is an example project on how to make a `pihole` + `unbound` DNS server on [balenaOS](https://www.balena.io/). This example uses `raspberry-pi` as the SBC, but it should be easily adaptable to other SBCs.

- [Pi-hole](https://pi-hole.net/): [https://gitlab.com/kmotoko/pihole-docker](gitlab.com/kmotoko/pihole-docker)
- [Unbound](https://unbound.net): [https://gitlab.com/kmotoko/unbound-docker](gitlab.com/kmotoko/unbound-docker)

## Host OS Config
Some of the important params to change/add in `/boot/config.json` (`balenaOS` host config file):
```json
{
  ...
  "country": "<2LETTERCOUNTRYCODE>",
  "dnsServers": "1.1.1.2 1.0.0.2",
  "hostname": "<HOSTNAME>",
  "ntpServers": "time.cloudflare.com europe.pool.ntp.org time.nist.gov",
  "sshKeys": [
    "ssh-rsa XXXXXXXX"
  ],
  ...
}
```

## Raspberry Pi Config (Recommended)
- Disable the interfaces potentially not needed and enable the watchdog:
```
# /boot/config.txt
dtparam=spi=off
dtparam=audio=off
dtparam=i2s=off
dtparam=watchdog=on
enable_uart=0
```
- If you want to have `DNSSEC` to work properly after power outages, long running offline modes etc. it is highly recommended to have an hardware clock (RTC module), as the `DNSSEC` validation is time-aware. Here we have `pcf8523` type hardware clock:
```
# /boot/config.txt
dtparam=i2c_arm=on
dtoverlay=i2c-rtc,pcf8523
```
In balenaOS, `rtc_pcf8523` kernel module should be loaded automatically after enabling it in the boot config. Check it: `lsmod | grep pcf`. Then, if your system time is up to date, set it in hwclock: `hwclock --systohc`. Restart the system, confirm that RTC is being used: `timedatectl`. Note that balena uses `chronyd` and it should auto sync from hwclock after power cuts.
- In some occasions (e.g. a power outage and come back), `pihole` might become up before the router interfaces (e.g. `eth0`, `eth1` ...) becomes available. In those cases, `pihole`'s `FTL` engine might struggle with certain issues. To prevent that you can add a boot delay:
```
# /boot/config.txt
boot_delay=30
```

## Robustness

### Watchdog
By default, `balenaos` configures `systemd` `watchdog` service.
- Check if you have `RuntimeWatchdogSec` defined in `/etc/systemd/system.conf.d/` or `/etc/systemd/system.conf`.
- Check if hardware watchdog enabled: `ls -al /dev/watchdog*`. You should see `watchdog` devices. For raspberry-pi devices, see above sections on how to enable it in boot config.
- If both are OK, then you are good.

### Container Healthcheck
By default, `pihole` uses [Docker healthchecks](https://github.com/pi-hole/docker-pi-hole/blob/master/src/Dockerfile) and exits if it fails. Thus, if you have `unless-stopped` or `always` in `restart` directive in the compose file, all is fine.

## Usage

### Pi-hole

For the `pihole` config details, see the related repo: https://gitlab.com/kmotoko/pihole-docker

### Unbound

This project includes an Unbound service providing DNS-over-TLS. For the `unbound` config details, see the related repo: https://gitlab.com/kmotoko/unbound-docker

**Note:** For security and footprint reasons, the Unbound container does not allow shell or terminal access via SSH or the balenaCloud console.

## Acknowledgements
This work was based on Kyle Harding's (from Balena team) repos and modified accordingly for specific use cases.

## Resources
You might find the following resources useful:
- [Kyle Harding's Balena Pihole Repo](https://github.com/klutchell/balena-pihole)
- [Kyle Harding's Archived Balena Pihole Repo](https://github.com/klutchell/balena-pihole-unbound)
- [Balena Blog Post on Pihole](https://www.balena.io/blog/deploy-network-wide-ad-blocking-with-pi-hole-and-a-raspberry-pi/)
